using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using ASPNoteBackend.Controllers;
using ASPNoteBackend.Model;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace ASPNoteBackend.Tests.ControllerTests;

public class NoteControllerTests
{
    private readonly IDbContextFactory<AppDbContext> _contextFactory;
    private readonly NoteController _controller;

    private static readonly NoteCreateRequest[] NoteRequests =
    {
        new()
        {
            Title = "Test Note Please Ignore",
            Content = "I told you to ignore me, didn't I?",
        },
        new()
        {
            Title = "I love potato knishes",
            Content = "This machine makes potato knishes",
        }
    };

    private readonly Note[] _notes =
    {
        new(NoteRequests[0]),
        new(NoteRequests[1]),
    };
    
    

    public NoteControllerTests()
    {
        var serviceCollection = new ServiceCollection();
        var root = new InMemoryDatabaseRoot();
        serviceCollection.AddPooledDbContextFactory<AppDbContext>(o => o.UseInMemoryDatabase("test", root));
        var services = serviceCollection.BuildServiceProvider()!;
        _contextFactory = services.GetRequiredService<IDbContextFactory<AppDbContext>>();
        _controller = new NoteController(_contextFactory);
        
    }

    [Fact]
    public async Task ListNotesWhenNoNotesShouldReturnEmptyListTest()
    {
        var result = await _controller.ListNotes();
        result.Should().BeEmpty();
    }

    [Fact]
    public async Task ListNotesWhenOnlyOneNoteExistsShouldReturnSingleNote()
    {
        await using var context = await _contextFactory.CreateDbContextAsync();
        await context.Notes.AddAsync(_notes[0]);
        await context.SaveChangesAsync();
        var result = await _controller.ListNotes();
        result.Should().ContainSingle().Which.Should().BeEquivalentTo(_notes[0]);
    }

    [Fact]
    public async Task ListNotesWhenOnlyOneNoteExistsDoesntReturnOtherNote()
    {
        await using var context = await _contextFactory.CreateDbContextAsync();
        await context.Notes.AddAsync(_notes[0]);
        await context.SaveChangesAsync();
        var result = await _controller.ListNotes();
        result.Should().ContainSingle().Which.Should().NotBeEquivalentTo(_notes[1]);
    }

    [Fact]
    public async Task GetNoteShouldReturnCorrectNote()
    {
        await using var context = await _contextFactory.CreateDbContextAsync();
        await context.Notes.AddRangeAsync(_notes);
        await context.SaveChangesAsync();
        var result = await _controller.GetNote(_notes[0].Id);
        result.Should().BeEquivalentTo(_notes[0]);
    }

    [Fact]
    public async Task CreateNote()
    {
        await _controller.CreateNote(NoteRequests[0]);
        await using var context = await _contextFactory.CreateDbContextAsync();
        var result = await context.Notes.SingleAsync();
        result.Title.Should().BeEquivalentTo(_notes[0].Title);
        result.Content.Should().BeEquivalentTo(_notes[0].Content);
    }

    [Fact]
    public async Task CreateEmptyNoteShouldReturnBadRequest()
    {
        // Arrange
        var empty = new NoteCreateRequest
        {
            Content = "",
            Title = "",
        }; 
        
        // Act
        var result = await _controller.CreateNote(empty);
        
        // Assert
        var actual = (BadRequestObjectResult)result.Result!;
        var badRequest = (int)HttpStatusCode.BadRequest;
        actual.StatusCode.Should().Be(badRequest);
    }

    [Fact]
    public async Task DeleteNoteThatExistsShouldDeleteNote()
    {
        await using var context = await _contextFactory.CreateDbContextAsync();
        context.Notes.AddRange(_notes);
        await context.SaveChangesAsync();

        var response = _controller.DeleteNote(_notes[0].Id);

        var actual = (OkResult)response.Result;
        var ok = (int)HttpStatusCode.OK;
        actual.StatusCode.Should().Be(ok);
        context.Notes.Should().NotContain(_notes[0]);
    }

    [Fact]
    public async Task DeleteNoteThatExistsShouldLeaveOtherNotes()
    {
        await using var context = await _contextFactory.CreateDbContextAsync();
        context.Notes.AddRange(_notes);
        await context.SaveChangesAsync();

        var response = _controller.DeleteNote(_notes[1].Id);

        var actual = (OkResult)response.Result;
        var ok = (int)HttpStatusCode.OK;
        actual.StatusCode.Should().Be(ok);
        context.Notes.Should().Contain(_notes[0]);
    }

    [Fact]
    public void DeleteNoteThatDoesntExistShouldReturnNotFound()
    {
        var response = _controller.DeleteNote(_notes[0].Id);
        
        var notFound = (int)HttpStatusCode.NotFound;
        var actual = (NotFoundResult)response.Result;
        actual.StatusCode.Should().Be(notFound);
    }

    [Fact]
    public void EditNoteThatDoesntExistShouldReturnNotFound()
    {
        var response = _controller.EditNote(NoteRequests[0], _notes[0].Id);

        var notFound = (int)HttpStatusCode.NotFound;
        var actual = (NotFoundResult)response.Result;
        actual.StatusCode.Should().Be(notFound);
    }

    [Fact]
    public async void EditNoteThatExistsShouldSaveChanges()
    {
        await using var context = await _contextFactory.CreateDbContextAsync();
        context.Add(_notes[0]);
        await context.SaveChangesAsync();

        var response = await _controller.EditNote(NoteRequests[1], _notes[0].Id);

        var ok = (int)HttpStatusCode.OK;
        var actual = (OkResult)response;
        actual.StatusCode.Should().Be(ok);
        
        var result = await context.Notes.SingleAsync();
        await context.Entry(result).ReloadAsync();
        result.Id.Should().Be(_notes[0].Id);
        result.Title.Should().Be(NoteRequests[1].Title);
        result.Content.Should().Be(NoteRequests[1].Content);
    }

    [Fact]
    public async Task ListNoteTitlesShouldReturnNoteTitles()
    {
        var noteTitles = new List<NoteTitle>();
        foreach (Note note in _notes)
        {
            noteTitles.Add(new NoteTitle(note));
        }

        await using var context = await _contextFactory.CreateDbContextAsync();
        await context.Notes.AddRangeAsync(_notes);
        await context.SaveChangesAsync();

        var response = await _controller.ListNoteTitles();

        var ok = (int)HttpStatusCode.OK;
        var actual = (OkObjectResult)response.Result!;
        actual.StatusCode.Should().Be(ok);
        var result = actual.Value as List<NoteTitle>;
        result.Should().ContainEquivalentOf(noteTitles[0]);
        result.Should().ContainEquivalentOf(noteTitles[1]);
    }

    [Fact]
    public async Task ListNoteTitlesShouldReturnEmptyListWhenNoNotes()
    {
        var response = await _controller.ListNoteTitles();

        var ok = (int)HttpStatusCode.OK;
        var actual = (OkObjectResult)response.Result!;
        actual.StatusCode.Should().Be(ok);
        var result = actual.Value as List<NoteTitle>;
        result.Should().BeEmpty();
    }
}