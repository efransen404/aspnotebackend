using ASPNoteBackend.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ASPNoteBackend.Controllers;

[ApiController]
[Route("[controller]")]
public class NoteController : ControllerBase
{
    private readonly IDbContextFactory<AppDbContext> _contextFactory;
    
    public NoteController(IDbContextFactory<AppDbContext> contextFactory)
    {
        _contextFactory = contextFactory;
    }

    [HttpGet]
    public async Task<IEnumerable<Note>> ListNotes()
    {
        await using var ctx = await _contextFactory.CreateDbContextAsync();
        return await ctx.Notes.ToListAsync();
    }

    [HttpGet("{id:guid}")]
    public async Task<Note?> GetNote(Guid id)
    {
        await using var ctx = await _contextFactory.CreateDbContextAsync();
        return await ctx.Notes.SingleOrDefaultAsync(note => note.Id == id);
    }

    [HttpPost]
    public async Task<ActionResult<Guid>> CreateNote([FromBody] NoteCreateRequest noteRequest)
    {
        if (noteRequest.Title == "") return BadRequest("Title field is empty");
        if (noteRequest.Content == "") return BadRequest("Content field is empty");
        await using var ctx = await _contextFactory.CreateDbContextAsync();
        var note = new Note(noteRequest);
        await ctx.AddAsync(note);
        await ctx.SaveChangesAsync();
        return Ok(note.Id);
    }

    [HttpDelete("{id:guid}")]
    public async Task<ActionResult> DeleteNote(Guid id)
    {
        await using var ctx = await _contextFactory.CreateDbContextAsync();
        var note = await ctx.Notes.SingleOrDefaultAsync(note => note.Id == id);
        if (note == null)
        {
            return NotFound();
        }
        ctx.Remove(note);
        await ctx.SaveChangesAsync();
        return Ok();
    }

    [HttpPost("{id:guid}")]
    public async Task<ActionResult> EditNote([FromBody] NoteCreateRequest body, Guid id)
    {
        await using var ctx = await _contextFactory.CreateDbContextAsync();
        if (await ctx.Notes.AnyAsync(note => note.Id == id)) 
        {
            var note = new Note(body, id); 
            ctx.Update(note);
            await ctx.SaveChangesAsync();
            return Ok();
        }
        return NotFound();
    }

    [HttpGet("/[controller]/list")]
    public async Task<ActionResult<NoteTitle[]>> ListNoteTitles()
    {
        await using var ctx = await _contextFactory.CreateDbContextAsync();
        var notes = await ctx.Notes
            .Select(note => new NoteTitle(note))
            .ToListAsync();
        return Ok(notes);
    }
}