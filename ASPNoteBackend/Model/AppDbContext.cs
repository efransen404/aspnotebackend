using Microsoft.EntityFrameworkCore;

namespace ASPNoteBackend.Model;

public class AppDbContext : DbContext
{
    public AppDbContext(DbContextOptions options) : base(options)
    {
        
    }

    public DbSet<Note> Notes { get; set; } = default!;

}