namespace ASPNoteBackend.Model;

public class Note
{
    public string Title { get; set; } = default!;

    public string Content { get; set; } = default!;

    public Guid Id { get; set; } = Guid.NewGuid();

    public Note()
    {
        
    }

    public Note(NoteCreateRequest noteCreateRequest)
    {
        this.Content = noteCreateRequest.Content;
        this.Title = noteCreateRequest.Title;
    }

    public Note(NoteCreateRequest noteCreateRequest, Guid id)
    {
        this.Content = noteCreateRequest.Content;
        this.Title = noteCreateRequest.Title;
        this.Id = id;
    }
}