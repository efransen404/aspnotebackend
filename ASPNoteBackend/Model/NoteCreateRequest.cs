namespace ASPNoteBackend.Model;

public class NoteCreateRequest
{
    public string Title { get; set; } = default!;

    public string Content { get; set; } = default!;
}