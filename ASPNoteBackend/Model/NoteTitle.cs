namespace ASPNoteBackend.Model;

public class NoteTitle
{
    public string Title { get; set; } = default!;

    public Guid Id { get; set; } = Guid.NewGuid();

    public NoteTitle(Note source)
    {
        Title = source.Title;
        Id = source.Id;
    }
}